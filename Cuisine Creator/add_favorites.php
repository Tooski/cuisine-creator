<?php
include 'db_connect.php';

            if(!$_POST['val']) {
                    $stmt = $db->prepare("INSERT INTO favorite (productName, username) VALUES (:productName, :username)");
                    $stmt->execute(array(':productName' => $_POST['prodName'], ':username' => $_POST['userName'])); 
            } else {
                  $stmt = $db->prepare("DELETE FROM favorite WHERE productName = :productName AND username = :username");
                    $stmt->execute(array(':productName' => $_POST['prodName'], ':username' => $_POST['userName']));
    
            }
            header("Location: my_recipes.php?product=".$_POST['prodName']);

?>
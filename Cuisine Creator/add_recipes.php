
<?php
include "db_connect.php";
include "header.php";
if (!isset($_SESSION['login'])) {
    header("Location: login_page.php");
}
?>

<style>

    iframe {background-color: white;};
    input[type=checkbox] { margin: 10px 5px 10px 10px;}
    input[type=number] {width: 75px;}
    .areatext {width : 100%;height : 100px; resize: none;}
    .inputText {width : 100%;}
    #nutritionTable{border-spacing: 0px; margin: 10px;}
    #nutritionTable td:not(#nutF) { padding: 0px; margin: 0px; border: 0px; border-bottom: 1px solid black;}
    #nutritionTable div { display:inline}
    #nutritionTable  input[type=number] { width:45px;}
    #nutritionTable  input[type=text]:not(#websource) { width:30px; }
    #tbmain tr td  {padding: 5px;}
    #tbmain { margin: 0px 5px; border: 5px #fdc442 solid; padding: 10px 20px; border-spacing: 0 10px;}
    #addr:hover {cursor: pointer;  } 
    #sm, #mm{ display: none; position: fixed; top: 0;  }
    #sm { left: 0; width: 100%; height: 100%;  background: black; opacity: 0.5;  z-index: 199; }
    #mm {  left: 25%; width: auto;  height: auto; z-index: 200;}
    .center { position: fixed; top: 50%; left: 50%; margin-top: -50px; margin-left: -100px;}
    #remove {float:right;}


</style>

<script>

    var heldValue = [];
    var prev = new Array();

    function removeRow(s)
    {
        var row = document.getElementById(s);
        row.parentNode.removeChild(row);
        removeValues(parseStr(s));
        heldValue.splice(heldValue.indexOf(parseStr(s)), 1);
        var ing = document.getElementById("ing");
        ing.options.add(new Option(s));
        sort(ing);

    }
    function sort(val) {
        var tmpAry = new Array();
        for (var i = 0; i < val.options.length; i++) {
            tmpAry[i] = new Array();
            tmpAry[i][0] = val.options[i].text;
            tmpAry[i][1] = val.options[i].value;
        }
        tmpAry.sort();
        while (val.options.length > 0) {
            val.options[0] = null;
        }
        for (var i = 0; i < tmpAry.length; i++) {
            var op = new Option(tmpAry[i][0], tmpAry[i][1]);
            val.options[i] = op;
        }
    }

    function submit() {
        var x = document.getElementById("ing").selectedIndex;
        if (x !== -1)
        {
            var y = document.getElementById("ing").options;

            var name = parseStr(y[x].text);
            var arrx = arrayss[name];

            if (arrx !== undefined && heldValue.indexOf(name) === -1)
            {
                var table = document.getElementById("ingTable");
                var row = table.insertRow(table.rows.length - 1);
                row.id = y[x].text;

                var c1 = row.insertCell(0);


                c1.innerHTML =
                        "<input type='number' value=" + arrx['servingSize']
                        + " name='" + name + "S' id='" + name
                        + "S' onchange='myFunction(\"" + name
                        + "\")' style='width: 45px;'><input type='text' value=" + arrx['servingMethod']
                        + " name='" + name + "M' id='" + name + "M'  style='width: 30px;'> of " + y[x].text +
                        "<input style=\"position:absolute; visibility:hidden\" type='text' value='" + y[x].text
                        + "' name='" + name + "A' id='" + name
                        + "A'  ><input type='button' id='remove' name ='remove' value='Remove " + y[x].text + "' onclick='removeRow(\"" + y[x].text + "\");'>";
                prev[name] = document.getElementById(name + "S").value;
                addValues(name);
                heldValue.push(name);
                document.getElementById("ing").remove(x);

            }
        }
    }

    function cancel() {

        document.getElementById('mm').style.display = 'none';
        document.getElementById('sm').style.display = 'none';

    }


    function parseStr(str)
    {
        return str.replace(/\s/g, '');
    }

    function addValues(val)
    {

        var arrx = arrayss[val];
        var s = parseInt(prev[parseStr(arrx['itemName'])]) / parseInt(arrx['servingSize']);
        var fat = document.getElementById("totalFat");
        fat.value = parseInt(fat.value) + parseInt(arrx['fat'] * s);
        document.getElementById("totalFatP").innerHTML = Math.ceil((parseInt(fat.value) / 65) * 100 * s);
        var cff = document.getElementById("saturatedFat");
        cff.value = parseInt(cff.value) + +parseInt(arrx['saturatedFat'] * s);
        document.getElementById("saturatedFatP").innerHTML = Math.ceil((parseInt(cff.value) / 20) * 100 * s);
        cff = document.getElementById("cholesterol");
        cff.value = parseInt(cff.value) + parseInt(arrx["cholesterol"] * s);
        document.getElementById("cholesterolP").innerHTML = Math.ceil((parseInt(cff.value) / 300) * 100 * s);
        cff = document.getElementById("sodium");
        cff.value = parseInt(cff.value) + parseInt(arrx["sodium"] * s);
        document.getElementById("sodiumP").innerHTML = Math.ceil((parseInt(cff.value) / 2400) * 100 * s);
        var carb = document.getElementById("carbs");
        carb.value = parseInt(carb.value) + parseInt(arrx["carbs"] * s);
        document.getElementById("carbsP").innerHTML = Math.ceil((parseInt(carb.value) / 300) * 100 * s);
        var df = document.getElementById("dietaryFiber");
        df.value = parseInt(df.value) + parseInt(arrx["dietaryFiber"] * s);
        document.getElementById("dietaryFiberP").innerHTML = Math.ceil((parseInt(df.value) / 25) * 100 * s);
        cff = document.getElementById("sugar");
        cff.value = parseInt(cff.value) + parseInt(arrx["sugars"] * s);
        var prot = document.getElementById("protein");
        prot.value = parseInt(prot.value) + parseInt(arrx["protein"] * s);
        cff = document.getElementById("calories");
        cff.value = parseInt(fat.value) * 9 + parseInt(prot.value) * 4 + (parseInt(carb.value) - parseInt(df.value)) * 4;
        cff = document.getElementById("caloriesFromFat");
        cff.value = parseInt(fat.value) * 9;
    }

    function updatePercents()
    {
        document.getElementById("sodiumP").innerHTML = Math.ceil((parseInt(document.getElementById("sodium").value) / 2400) * 100);
        document.getElementById("saturatedFatP").innerHTML = Math.ceil((parseInt(document.getElementById("saturatedFat").value) / 20) * 100);
        document.getElementById("cholesterolP").innerHTML = Math.ceil((parseInt(document.getElementById("cholesterol").value) / 300) * 100);
        var carb = document.getElementById("carbs");
        document.getElementById("carbsP").innerHTML = Math.ceil((parseInt(carb.value) / 300) * 100);
        var df = document.getElementById("dietaryFiber");
        document.getElementById("dietaryFiberP").innerHTML = Math.ceil((parseInt(df.value) / 25) * 100);
        var fat = document.getElementById("totalFat");
        document.getElementById("totalFatP").innerHTML = Math.ceil((parseInt(fat.value) / 65) * 100);
        var prot = document.getElementById("protein");
        var cff = document.getElementById("calories");
        cff.value = parseInt(fat.value) * 9 + parseInt(prot.value) * 4 + (parseInt(carb.value) - parseInt(df.value)) * 4;
        cff = document.getElementById("caloriesFromFat");
        cff.value = parseInt(fat.value) * 9;

    }

    function removeValues(val)
    {
        var arrx = arrayss[val];

        var s = parseInt(prev[parseStr(arrx['itemName'])]) / parseInt(arrx['servingSize']);
        var m = arrx['servingMethod'];
        var fat = document.getElementById("totalFat");
        fat.value = parseInt(fat.value) - parseInt(arrx['fat'] * s);
        document.getElementById("totalFatP").innerHTML = Math.ceil((parseInt(fat.value) / 65) * 100 * s);
        var cff = document.getElementById("saturatedFat");
        cff.value = parseInt(cff.value) - parseInt(arrx['saturatedFat'] * s);
        document.getElementById("saturatedFatP").innerHTML = Math.ceil((parseInt(cff.value) / 20) * 100 * s);
        cff = document.getElementById("cholesterol");
        cff.value = parseInt(cff.value) - parseInt(arrx["cholesterol"] * s);
        document.getElementById("cholesterolP").innerHTML = Math.ceil((parseInt(cff.value) / 300) * 100 * s);
        cff = document.getElementById("sodium");
        cff.value = parseInt(cff.value) - parseInt(arrx["sodium"] * s);
        document.getElementById("sodiumP").innerHTML = Math.ceil((parseInt(cff.value) / 2400) * 100 * s);
        var carb = document.getElementById("carbs");
        carb.value = parseInt(carb.value) - parseInt(arrx["carbs"] * s);
        document.getElementById("carbsP").innerHTML = Math.ceil((parseInt(carb.value) / 300) * 100 * s);
        var df = document.getElementById("dietaryFiber");
        df.value = parseInt(df.value) - parseInt(arrx["dietaryFiber"] * s);
        document.getElementById("dietaryFiberP").innerHTML = Math.ceil((parseInt(df.value) / 25) * 100 * s);
        cff = document.getElementById("sugar");
        cff.value = parseInt(cff.value) - parseInt(arrx["sugars"] * s);
        var prot = document.getElementById("protein");
        prot.value = parseInt(prot.value) - parseInt(arrx["protein"] * s);
        cff = document.getElementById("calories");
        cff.value = parseInt(fat.value) * 9 + parseInt(prot.value) * 4 + (parseInt(carb.value) - parseInt(df.value)) * 4;
        cff = document.getElementById("caloriesFromFat");
        cff.value = parseInt(fat.value) * 9;

    }


    function myFunction(val)
    {
        var name = parseStr(val);
        removeValues(name);
        prev[name] = document.getElementById(name + "S").value;
        addValues(name);
    }
    var arrayss = new Array();
    var cr = new Array();
    function createRow(val)
    {
        var recipe = eval("(" + val + ")");
        arrayss[parseStr(recipe['itemName'])] = recipe;
    }


</script>



<tr>
    <td>
        <form  action="create_recipe.php" method="post" enctype="multipart/form-data">
            <center>  <table  border="0" id="tbmain">
                    <tr>

                        <td><h1 style="text-transform: uppercase"><center>Add Recipe</center></h1>
                            <input id="recipeName" name="recipeName" placeholder="Recipe Name" class="inputText"/>
                        </td>
                    </tr>
                    <tr>
                        <td><hr width='100%' style="margin:10px 0px 15px 0px;" >Description:
                            <p><textarea type="text" id="description" name="description" placeholder="Description" class="areatext"></textarea></p>
                            <p>
                                Add picture: <input type="file" name="file" id="file"> 
                            </p></td>
                    </tr>

                    <tr>
                        <td>



                            <p><hr width='100%' style="margin:10px 0px 15px 0px;" >All Ingredients:</p>
                            <p><?php
                                echo "<center><table width = '100%' id='ingTable'>";

                                echo "<tr>
                                        <td>
                                    <div id='sm'></div><div id='mm'><table class='center' width='250' height='250'><tr><td colspan='2'><center><select size='8' id = 'ing'>";

                                foreach ($db->query("SELECT productName FROM product") as $row) {
                                    echo "<option value='" . $row['productName'] . "' id='" . $row['productName'] . "Q' name='" . $row['productName'] . "Q'>" . $row['productName'] . "</option>";
                                }

                                foreach ($db->query("SELECT * FROM nutrition") as $row) {
                                    echo "<script type=\"text/javascript\"> createRow('" . json_encode($row) . "');</script>";
                                }

                                echo "</select></center></td></tr><tr><td align='center'><a href='javascript:cancel();'>Cancel</a></td><td align='center'><a href='javascript:submit();'>Submit</a></td></tr></table></div><center><input type='button' id='addr' name ='addr' value='Add Ingredient'></center></td></tr>";
                                echo "</table></center>";
                                ?></p></td> 
                    </tr>
                    <script>
                        fx('mm', 'sm', 'addr');

                        function p(var1, var2) {
                            document.getElementById(var1).style.display = var2;
                        }

                        function q(var1)
                        {
                            document.getElementById(var1).style.position = 'absolute';
                        }


                        function fx(var1, var2, var3)
                        {
                            document.getElementById(var3).onclick = function() {
                                p(var1, 'block');
                                p(var2, 'block');
                            };
                            document.getElementById(var2).onclick = function() {
                                p(var1, 'none');
                                p(var2, 'none');
                            };
                            if (('maxHeight' in document.body.style) === false) {
                                q(var1);
                                q(var2);
                            }
                        }


                    </script>
                    <tr>
                    <tr>
                        <td><p><hr width='100%' style="margin:10px 0px 15px 0px;" >Directions:</p>
                            <p><textarea type="text" id="direction" name="direction" class="areatext"></textarea></p></td>
                    </tr>
                    <tr>
                        <td>
                    <center><hr width='100%' style="margin:10px 0px 15px 0px;" ><table width="604" id="nutritionTable">
                            <tr>
                                <td colspan="3" id="nutF"><h2>Nutrition Facts</h2></td>
                            </tr>
                            <tr>
                                <td colspan="3">Serving Size <input name="servingSize" type="number" id="servingSize"  value="0" ><input name="servingMethod" type="text" id="servingMethod"  value="g" >
                                </td>
                            </tr>
                            <tr>
                                <td height="13" colspan="3" bgcolor="#000000"></td>
                            </tr>
                            <tr>
                                <td colspan="3"><strong>Amount Per Serving</strong></td>
                            </tr>
                            <tr>
                                <td colspan="2"><strong>Calories </strong><input name="calories" type="number" id="calories"  value="0" step="any"></td>
                                <td width="330" align="right">Calories from Fat <input name="caloriesFromFat" type="number" id="caloriesFromFat"  value="0" step="any"></td>
                            </tr>
                            <tr>
                                <td height="4" colspan="3" bgcolor="#000000"></td>
                            </tr>
                            <tr>
                                <td colspan="3" align="right"><strong>% Daily Value*</strong></td>
                            </tr>
                            <tr>
                                <td colspan="2"><strong>Total Fat </strong><input name="totalFat" type="number" id="totalFat" value="0" onchange="updatePercents()" step="any"> g</td>
                                <td align="right"><div id="totalFatP">0</div>%</td>
                            </tr> 
                            <tr>
                                <td width="20">&nbsp;</td>
                                <td width="246">Saturated Fat <input name="saturatedFat" type="number" id="saturatedFat" value="0" onchange="updatePercents()" step="any"> g</td>
                                <td  align="right"><div id="saturatedFatP">0</div>%</td>
                            </tr>
                            <tr>
                                <td colspan="2"><strong>Cholesterol</strong> <input name="cholesterol" type="number" id="cholesterol" value="0" onchange="updatePercents()" step="any"> mg</div></td>
                                <td  align="right"><div id="cholesterolP">0</div>%</td>
                            </tr>
                            <tr>
                                <td colspan="2"><strong>Sodium</strong> <input name="sodium" type="number" id="sodium" value="0" onchange="updatePercents()" step="any"> mg</td>
                                <td align="right"><div id="sodiumP">0</div>%</td>
                            </tr>
                            <tr>
                                <td colspan="2"><strong>Total Carbohydrates </strong></div><input name="carbs" type="number" id="carbs" value="0" onchange="updatePercents()" step="any"> g</td>
                                <td align="right"><div id="carbsP">0</div>%</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>Dietary Fiber <input name="dietaryFiber" type="number" id="dietaryFiber" value="0" onchange="updatePercents()" step="any"> g</td>
                                <td align="right"><div id="dietaryFiberP">0</div>%</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2">Sugars <input name="sugar" type="number" id="sugar" value="0" onchange="updatePercents()" step="any"> g</td>
                            </tr>
                            <tr>
                                <td colspan="3"><strong>Protein </strong><input name="protein" type="number" id="protein"  value="0" onchange="updatePercents()" step="any"> g</td>
                            </tr>
                            <tr><td colspan="3" id="websrc" name="websrc"><input type="text" name="websource" id="websource" placeholder="Web Source" class="inputText" step="any"></td></tr>

                        </table></center></td>
                    </tr>
                    <tr>
                        <td><p><center><hr width='100%' style="margin:10px 0px 15px 0px;" >Tags:</center><?php
                    $tt = "<table cellpadding=\"50\">";
                    echo "<center>" . str_replace('\'', '"', $tt);
                    $intv = 0;
                    foreach ($db->query("SELECT category FROM category") as $row) {
                        $intv++;
                        echo "<td><label><input type=\"checkbox\" name=\"" . $row['category'] . "\" value=\"" . $row['category'] . "\">" . $row['category'] . "</input></label></td>";
                        if ($intv % 4 == 0) {
                            echo "<tr></tr>";
                        }
                    }
                    echo"</table></center>"
                    ?>

                    </p></td>
                    </tr>
                    <tr>
                        <td><center>
                        <hr width='100%' style="margin:10px 0px 15px 0px;" >
                        <input type="submit"/></center></td>
                    </tr>
                </table>
        </form>
        <p id="div_footer">
            ALL RIGHTS RESERVED, Copyright 2013 | Cuisine Creator &nbsp;&nbsp; 
            Website created by Joe Nosov, Thomas Nunn, Cathryn Castillo
        </p>
    </td>
</tr>
</table>
</body>
</html>

<?php

include "db_connect.php";
$stmt = $db->prepare("INSERT INTO productUser (productName, username, timeCreated, dateCreated) VALUES (:productName, :username, :timeCreated, :dateCreated);");
$stmt->execute(array(':productName' => $_REQUEST['recipeName'], ':username' => $_SESSION['login'][0], ':timeCreated' => strftime("%r %Z"), ':dateCreated' => strftime("%D")));


$stmt = $db->prepare("INSERT INTO nutritionSource (webSource, productName) VALUES (:webSource, :productName);");
$stmt->execute(array(':webSource' => $_REQUEST['websource'], ':productName' => $_REQUEST['recipeName']));



$query = $db->prepare("SELECT productName FROM product");
$query->execute();
for ($i = 0; $row = $query->fetch(); $i++) {
    $value = str_replace(" ", "", $row['productName']);
    if (isset($_REQUEST[$value . "A"])) {
        $stmt = $db->prepare("INSERT INTO recipe (recipeName, ingredientName,quantity,measurement) VALUES (:recipeName, :ingredientName, :quantity, :measurement)");
        $stmt->execute(array(':recipeName' => $_REQUEST['recipeName'], ':ingredientName' => $_REQUEST[$value . "A"], ':quantity' => $_REQUEST[$value . "S"], ':measurement' => $_REQUEST[$value . "M"]));
    }
}



if (isset($_REQUEST['direction']) && strlen($_REQUEST['direction']) > 0) {
    $stmt = $db->prepare("INSERT INTO directions (direction, productName) VALUES (:direction, :productName);");
    $stmt->execute(array(':direction' => $_REQUEST['direction'], ':productName' => $_REQUEST['recipeName']));
}


if (in_array(end(explode(".", $_FILES["file"]["name"])), array("jpg", "jpeg", "pdd", "tiff", "gif", "pict", "png", "psd", "bmp"))) {
    $dne = move_uploaded_file($_FILES["file"]["tmp_name"], "uploads/" . $_FILES["file"]["name"]);
}

if ($dne) {
    $stmt = $db->prepare("INSERT INTO product (productName, description, picSource) VALUES (:productName, :description, :picSource);");
    $stmt->execute(array(':productName' => $_REQUEST['recipeName'], ':description' => $_REQUEST['description'], ':picSource' => $_FILES["file"]["name"]));
} else {
    if (isset($_REQUEST['description']) && strlen($_REQUEST['description']) > 0) {
        $stmt = $db->prepare("INSERT INTO product (productName, description) VALUES (:productName, :description);");
        $stmt->execute(array(':productName' => $_REQUEST['recipeName'], ':description' => $_REQUEST['description']));
    }
}

$stmt = $db->prepare("INSERT INTO product(productName,description,picSource) VALUES(:productName,:description,:picSource)");
$stmt->execute(array(':productName' => $_REQUEST['recipeName'], ':description' => "Test", ':picSource' => "test"));
$stmt = $db->prepare("INSERT INTO nutrition(itemName,servingSize,servingMethod,calories,caloriesFromFat,fat,saturatedFat,"
        . "cholesterol,sodium,carbs,dietaryFiber,sugars,protein) VALUES "
        . "(:recipeName,:servingSize,:servingMethod,:calories,:caloriesFromFat,:totalFat,:saturatedFat,:cholesterol"
        . ",:sodium,:carbs,:dietaryFiber,:sugar,:protein)");
$stmt->execute(array(
    ':recipeName' => $_REQUEST['recipeName'],
    ':servingSize' => $_REQUEST['servingSize'],
    ':servingMethod' => $_REQUEST['servingMethod'],
    ':calories' => $_REQUEST['calories'],
    ':caloriesFromFat' => $_REQUEST['caloriesFromFat'],
    ':totalFat' => $_REQUEST['totalFat'],
    ':saturatedFat' => $_REQUEST['saturatedFat'],
    ':cholesterol' => $_REQUEST['cholesterol'],
    ':sodium' => $_REQUEST['sodium'],
    ':carbs' => $_REQUEST['carbs'],
    ':dietaryFiber' => $_REQUEST['dietaryFiber'],
    ':sugar' => $_REQUEST['sugar'],
    ':protein' => $_REQUEST['protein']
));

foreach ($db->query("SELECT category FROM category") as $row) {
    if (isset($_REQUEST[$row['category']])) {
        $stmt = $db->prepare("INSERT INTO productCategory (category, productName) VALUES (:category, :productName)");
        $stmt->execute(array(':category' => $row['category'], ':productName' => $_REQUEST['recipeName']));
    }
}
$nutrition = "INSERT INTO nutrition VALUES (\"" . $_REQUEST['recipeName'] . "\","
        . $_REQUEST['servingSize'] . ", \"" . $_REQUEST['servingMethod'] . "\","
        . $_REQUEST['calories'] . ","
        . $_REQUEST['caloriesFromFat'] . ","
        . $_REQUEST['totalFat'] . ","
        . $_REQUEST['saturatedFat'] . ","
        . $_REQUEST['cholesterol'] . ","
        . $_REQUEST['sodium'] . ","
        . $_REQUEST['carbs'] . ","
        . $_REQUEST['dietaryFiber'] . ","
        . $_REQUEST['sugar'] . ","
        . $_REQUEST['protein'] . ");";
$result = $db->exec($nutrition);
$insertId = $db->lastInsertId();

header("Location: my_recipes.php?product=" . $_REQUEST['recipeName']);
?>
<?php
$recipe_name = "";
$recipe_description = "";
$pic_name = "";
$stars = "";

    function display_rotd($db_con) {
    
        if (isset($db_con)) {
            
            foreach($db_con->query('SELECT * FROM product ORDER BY RAND()LIMIT 3') as $row) {
                
                global $recipe_name;
                $recipe_name = $row['productName'];
                global $recipe_description;
                $recipe_description = (strlen($row['description']) > 200 ? substr(substr($row['description'], 0, 200), 0, strrpos(substr($row['description'], 0, 200), ' ')) :$row['description']);
                global $pic_name;
                $pic_name = $row['picSource'];
                

                
                global $stars;
                foreach ($db_con->query("SELECT AVG(stars) FROM review where productName='" . $recipe_name . "';") as $value) {
                      $stars = ceil($value['AVG(stars)']);
                }

               
                ?>
                
                <!-- Diplay each recipe in a table. The form sends the recipe's name to recipes.php -->
                <div class="rotd">
                
                  <?php   echo '<form action="my_recipes.php?product='.$recipe_name.'" method="post"> <input type="hidden" name="recipe_name" 
                    value="'.htmlspecialchars($recipe_name).'">';
                    ?>
                        <table cellspacing="0" style="vertical-align: top;" width="100%">
                            <tr>
                                <td style="vertical-align: top;"><?php echo "<a href='my_recipes.php?product=".$recipe_name."'><strong>".strtoupper($recipe_name)."</a>" ?></td>
                            </tr>
                            <tr>
                                <td>
                                    <?php 
                                    if($stars)
                                    {
                                    for ($i=0; $i<$stars; $i++) {
                                        echo '<img src="images/stars.png" width="30">';
                                    }
                                         for ($i = $stars; $i < 5; $i++) {
                                         echo '<img src="images/starsT.png" width="30">';
                                }
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr> 
                                <td><?php if(file_exists("uploads/".$pic_name) && isset($pic_name)) { echo '<input type="image" src="uploads/'.$pic_name.'" alt="'.$pic_name.' Picture" style="width:200px;" >'; }; ?></td>
                            </tr>
                            <tr>
                                <td><?php echo $recipe_description ?></td>
                            </tr>
                       </table>
                    </form>
                </div>
                
                <?php
            }
            
        }
        
    }
?>

<?php
    session_start();
    include 'functions.php';
    include 'recipes_of_the_day.php';
    

    if (isset($_SESSION['login'])) {
        $userName = $_SESSION['login'][0];
        $displayName = $_SESSION['login'][1];
    }
    
    if (isset($_POST['logout'])) {
        logout();
    }
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="styles/styles.css">
        <title>Cuisine Creator</title>
    </head>
    <body>
        <table id="master_table">
            <tr><!--row 1-->
                <td>
                    <!--begin page header-->     
                    <div id="top-header">
                            <div id="logo-div"><a href="index.php"><img id="logo" src="images/cclogo.png"></a></div>
                            <?php if (isset($displayName)) {
                                echo '<div class="title_table_blank">Welcome, '.$displayName.'!  |  <a href="my_account.php">My Account</a> <br />
                                    <form action="header.php" method="post"><button type="submit" name="logout">Log Out</button></form>
                                </div>';
                            } else {
                                echo '<div class="title_table_blank"><a href="login_page.php">login</a> 
                                	| <a href="login_page.php">sign up</a></div>';
                            }
                            ?>
                            <div class="clear"></div>
                    </div>
                    <table class="nav_bar">
                        <tr>
                            <td><a href="recipes.php"><div class="nav_div">All Recipes</div></a></td>
                            <td><a href="add_recipes.php"><div class="nav_div">Add Recipes</div></a></td>
                            <td><a href="favorites.php"><div class="nav_div">Favorites</div></a></td>
                            <td><a href="about.php"><div class="nav_div">About Us</div></a></td>
                            <td><a href="contact.php"><div class="nav_div">Contact Us</div></a></td>
                            <td>
                                <form action="recipes.php" method="post">
                                <input type="text" name="search" placeholder="Search Cuisine Creator...">
                                <button type="submit" name="basic-search">Search</button>
                                </form>
                            </td>
                        </tr>
                    </table>
                    <!--end page header-->
                </td>
            </tr> <!-- end row 1 -->
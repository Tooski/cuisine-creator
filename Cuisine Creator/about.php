<?php
    include 'header.php';
?>

            <tr><!-- row 2 -->
                <td>
                    <table class="about">
                        <tr><td><h1>About Cuisine Creator</h1></td></tr>
                        <tr><td></td></tr>
                        <tr>
                            <td>
                                <p>
                                    Welcome to Cuisine Creator, your one-stop source for recipes. 
                                    Our team of highly dedicated developers strive each and every day to 
                                    bring you the very best menu design tool on the web. Give us a try, you won't be disappointed.
                                </p>
                            </td>
                        </tr>
                        <tr><td><h2>What We Do</h2></td></tr>
                        <tr>
                            <td>
                                <p>
                                    Here at Cuisine Creator we offer a variety of user created recipes to help you plan,
                                    budget for, and implement your daily menu. From world class cuisine to your Grandma's fried chicken, Cuisine Creator has it all.
                                    We don't stop there, however. Anyone can sign up for a free account and begin creating their own recipes or just put their
                                    spin on an already existing recipe.
                                </p>
                            </td>
                        </tr>
                        <tr><td><h2>Testimonials</h2></td></tr>
                        <tr>
                            <td><p>"Without Cuisine Creator I'd still be working at Burger King"<p style="font-style: italic;">Bobby Flay</p></p></td>
                        </tr>
                        <tr>
                            <td><p>"I just can't live without without Cuisine Creator! Oh wait, I'm dead."<p style="font-style: italic;">Julia Child</p></p></td>
                        </tr>
                        <tr>
                            <td><p>"You bloody well better sign up for Cuisine Creator! Don't make me come to your kitchen."<p style="font-style: italic;">Gordon Ramsay</p></p></td>
                        </tr>
                        <tr>
                            <td><p>"I like to down a fifth of some choice whiskey and spend hours surfing Cuisine Creator with an escort girl."<p style="font-style: italic;">Anthony Bourdain</p></p></td>
                        </tr>
                            <td><p>"Y'all should get some bacon and cream and a bucket of lard and start trying some recipes on Cuisine Creator!"<p style="font-style: italic;">Paula Deen</p></p></td>
                        </tr>
                        <tr>
                            <td><p>"Cuisine Creator helps me to Live, Love, and Eat!"<p style="font-style: italic;">Wolfgang Puck</p></p></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr><!-- row 3 -->
                    <td>
                        <p id="div_footer">
                        ALL RIGHTS RESERVED, Copyright 2013 | Cuisine Creator &nbsp;&nbsp; 
                        Website created by Joe Nosov, Thomas Nunn, Cathryn Castillo
                        </p>
                    </td>
            </tr>
         </table>
    </body>
</html>
        
<?php
    include 'header.php';
?>
           
            <tr><!--row 2-->
                <td class="row_2_table_parent_td" style="vertical-align: top;">
                    <table class="row_2_table">
                        <tr>
                            <td  style="vertical-align: top;">

                                <script> 
                                    MySlides=new Array('title_pic.jpg','f51.jpg','plated_food.jpg','meal.jpg','meal_2.jpg','meal_3.jpg');
                                    Slide=0;
                                    // ShowSlides called every 5 seconds
                                    setInterval(function ShowSlides(){
                                        if (Slide>MySlides.length-1){
                                            Slide=0;
                                        }
                                        document.DisplaySlide.src=MySlides[Slide];
                                        Slide++;
                                    }, 5000);
                                </script>
                        
                                <table id="div_pic_slider" cellspacing="0">
                                    <tr>
                                        <td style="vertical-align: top;">
                                            <a href="index.php"><img src="title_pic.jpg" name="DisplaySlide"/></a>
                                        </td>
                                    </tr>
                                </table>
                                <div class="div_four_options">
                                			<h1>Learn the Basics</h1>
                                            <a href="add_recipes.php"><h3>Create a Recipe</h3></a>
                                            <p>
                                                    Build a recipe from scratch using our extensive list of 
                                                    ingredients. With Cuisine Creator there is no longer a 
                                                    need for pen and paper. On a mobile device you can enter 
                                                    and save your new recipe while your making it!
                                            </p>
                                            
                                            <a href="recipes.php"><h3>Modify an Existing Recipe</h3></a>
                                            <p>
                                                    Have you ever tried a recipes and thought 'this needs
                                                    more salt'? Now you can modify any recipe on Cuisine
                                                    Creator and make it your own. Pick a recipe as a
                                                    template to start with and then add or delete ingredients
                                                    as needed.
                                            </p>
                                        
                                            <a href="favorites.php"><h3>View Favorites</h3></a>
                                            <p>
                                                    Check out all your favorite recipes whenever you're
                                                    working on a meal plan. With Cuisine Creator there
                                                    is no more need for dozens of recipe books or index
                                                    card boxes with handwritten recipes from grandma.
                                            </p>
                                            
                                            <a href="recipes.php"><h3>Search by Ingredient</h3></a>
                                            <p>
                                                    Search our extensive database of recipes by any
                                                    ingredient. If there's an ingredient missing that
                                                    you think we should have, just fill out the handy
                                                    ingredient request form. Our culinary experts will
                                                    review your request within 24 hours.
                                            </p>
                            	</div>                		
                            </td>
                            <td class="rotd_container">
                                <h2>Random Products</h2>
                                
                                        <?php
                                            $db = db_connect();
                                            display_rotd($db);
                                        ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr><!-- row 3 -->
                    <td>
                        <p id="div_footer">
                        ALL RIGHTS RESERVED, Copyright 2013 | Cuisine Creator &nbsp;&nbsp; 
                        Website created by Joe Nosov, Thomas Nunn, Cathryn Castillo
                        </p>
                    </td>
            </tr>
        </table>
    </body>
</html>
<?php

    include 'header.php';
                    
?>

            <tr><!-- row 2 -->
                <td>
                    <table class="about">
                        <form action="contact.php" method="post">
                            <tr>
                                <td><h1>Contact Us</h1></td>
                            </tr>
                            <tr>
                                <td>
                                    <p>
                                        We want your feedback so we can give you the best experience possible. We know our members have great ideas,
                                        so please share yours with us. Tell us what you like and don't like, and what changes you would like to see.
                                        We'll respond to all messages within 24 hours. You have our guarantee!
                                    </p>
                                </td>
                            </tr>
                            <tr><td>Send us a message:</td></tr>
                            <tr>

                                <td>
                                    <textarea name="message" style="width: 100%; height: 300px;"></textarea>
                                </td>
                            </tr>
                            <tr><td><button type="submit" name="send">Send</button></td></tr>
                        </form>
                    </table>
                </td>
            </tr>
            <tr><!-- row 3 -->
                    <td>
                        <p id="div_footer">
                        ALL RIGHTS RESERVED, Copyright 2013 | Cuisine Creator &nbsp;&nbsp; 
                        Website created by Joe Nosov, Thomas Nunn, Cathryn Castillo
                        </p>
                    </td>
            </tr>
         </table>
    </body>
</html>
        
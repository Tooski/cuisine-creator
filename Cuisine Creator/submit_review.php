<?php
include "db_connect.php";

$stmt = $db->prepare("INSERT INTO review (stars, productName, review, username, name) VALUES (:stars, :productName, :review, :username, :name);");
$stmt->execute(array(':stars' => ($_POST['starcount']+1), ':productName' => $_POST['prod'], ':review' => $_POST['review'], ':username' =>  $_SESSION['login'][0], ':name' => $_SESSION['login'][1]));
header("Location: my_recipes.php?product=".$_POST['prod']);

?>
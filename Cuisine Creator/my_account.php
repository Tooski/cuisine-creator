<?php
    include 'header.php';

    $db = db_connect();
    if (isset($db)) {
        $stmt = $db->prepare("SELECT * FROM user WHERE username = ?");
        $stmt->execute(array($userName)); // userName is in header.php
        $result_array = $stmt->fetch(PDO::FETCH_ASSOC);
        
        $display_name = $result_array['name'];
        $password = $result_array['password'];
    }
    
    $new_password = "";
    $new_display_name = "";
    $pass_change_success = "";
    $display_name_change_success = "";
    
    if (isset($_POST['password']) && isset($_POST['password_confirm'])) {
        
        if (empty($new_display_name)) {
            $new_display_name = $display_name;
        }
        
        if ($_POST['password'] === $_POST['password_confirm']) {
            $new_password = $_POST['password'];
                if (isset($db)) {
                    $stmt = $db->prepare("UPDATE user SET name = ?, password = ? WHERE username = ?");
                    $stmt->execute(array($new_display_name, $new_password, $userName));
                    $pass_change_success = 'Password Changed!';
                    $_SESSION['login'][1] = $new_display_name;
                }
        }
    }
        
 
    
    if (isset($_POST['display_name']) && isset($_POST['display_name_confirm'])) {
        
        if (empty($new_password)) {
            $new_password = $password; // user didn't change their password
        }
        
        if ($_POST['display_name'] === $_POST['display_name_confirm']) {
            $new_display_name = $_POST['display_name'];
                if (isset($db)) {
                    $stmt = $db->prepare("UPDATE user SET name = ?, password = ? WHERE username = ?");
                    $stmt->execute(array($new_display_name, $new_password, $userName));
                    $display_name_change_success = 'Display Name Changed!';
                    $_SESSION['login'][1] = $new_display_name;
                }
        }
    } 
?>

            <tr>
                <td style="width: 1100px; border: 1px white solid;"></td>
            </tr>
            <tr><!-- row 2 -->
                <td>
                    
                        <p style="text-align: center; color: #660066; font-size: 130%;">MY ACCOUNT</p>
                        <table class="my_account_table" cellspacing="10">
                            <form action="my_account.php" method="post">
                                <tr>
                                    <td><p style="color: #660066; font-size: 125%;">Change My Information</p></td>

                                </tr>
                                <tr>
                                    <td style="text-align: left;"><p><strong>Name:</strong><p style="font-size: 120%; font-style: italic; color: #990000;">
                                        <?php echo $userName ?></p></p></td>
                                </tr>
                                <tr style="background-color: ghostwhite;">
                                    <td style="text-align: left;"><p><strong>Password:</strong>&nbsp;
                                        <?php 
                                            $chars = str_split($password);
                                            foreach ($chars as $char)  {
                                            echo '*';
                                        }?>
                                    </p></td>
                                    <td><input type="text" name="password" placeholder="new password"></td>
                                    <td><input type="text" name="password_confirm" placeholder="confirm password"></td>
                                    <td>
                                        <?php
                                            if (!empty($pass_change_success)) {
                                                echo $pass_change_success;
                                            }
                                            $pass_change_success = "";
                                            ?>
                                    </td>
                                    <td style="text-align: center;"><button type="submit">Submit</button></td>
                                </tr>
                            </form>
                            <form action="my_account.php" method="post">
                                <tr style="background-color: ghostwhite;">
                                    <td style="text-align: left;"><p><strong>Display Name:</strong>&nbsp;<?php echo $display_name;?></p></td>
                                    <td><input type="text" name="display_name" placeholder="new display name"></td>
                                    <td><input type="text" name="display_name_confirm" placeholder="confirm display name"></td>
                                    <td>
                                        <?php
                                            if (!empty($display_name_change_success)) {
                                                echo $display_name_change_success;
                                            }
                                            $display_name_change_success = "";
                                        ?>
                                    </td>
                                    <td style="text-align: center;"><button type="submit">Submit</button></td>
                                </tr>
                            </form>
                            <tr>
                                <td></td>
                                
                            </tr>
                            <tr>
                                <td>
                                </td>
                            </tr>
                        </table>
                    
                </td>
            </tr>
            <tr><!-- row 3 -->
                    <td>
                        <p id="div_footer">
                        ALL RIGHTS RESERVED, Copyright 2013 | Cuisine Creator &nbsp;&nbsp; 
                        Website created by Joe Nosov, Thomas Nunn, Cathryn Castillo
                        </p>
                    </td>
            </tr>
         </table>
    </body>
</html>

<?php
    session_start();
    include 'functions.php';

    $userNameInput = "";
    $passInput = "";
    $_SESSION['passERR'] = "";
    $_SESSION['signpassERR'] = "";
    
    
    function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    if (!isset($_SESSION['login'])) {

        if ($_SERVER["REQUEST_METHOD"] == "POST" && empty($_POST["sign_up_username"]) 
                && empty($_POST["sign_up_password"]) && empty($_POST["sign_up_displayname"])) { // user is trying to log in

            if (empty($_POST["username"]) || empty($_POST["password"])) {
                $_SESSION['passERR'] = "Please enter both fields";
                header('Location: login_page.php');
                exit();
            } else {
                $userNameInput = test_input($_POST["username"]);
                $passInput = test_input($_POST["password"]);
            }
 
// Query database to see if user exists
            
            $db = db_connect();
            
            if (isset($db)) {
            
                $stmt = $db->prepare("SELECT * FROM user WHERE username = ? AND password = ?");
                $stmt->execute(array($userNameInput, $passInput));
                $result_array = $stmt->fetch(PDO::FETCH_ASSOC);
                $userName = $result_array['username'];
                $displayName = $result_array['name'];

                if (isset($userName)) { // log in successful, start new session
                    $_SESSION = array();
                    session_destroy();
                    session_start();
                    $loginArray = array($userName, $displayName);
                    $_SESSION['login'] = $loginArray;
                    header('Location: index.php');
                    exit();
                } else {
                    $_SESSION['passERR'] = "Incorrect Name or Password!";
                    header('Location: login_page.php');
                    exit();
                }
            } 
        } else if ($_SERVER["REQUEST_METHOD"] == "POST" && empty($_POST["username"]) 
                && empty($_POST["password"])){ // user is trying to sign up

            if (empty($_POST["sign_up_username"]) || empty($_POST["sign_up_password"]) 
                    || empty($_POST["sign_up_displayname"])) {
                $_SESSION['signpassERR'] = "Please enter all 3 fields";
                header('Location: login_page.php');
                exit();
            } else {
                global $sign_up_name;
                $sign_up_name = test_input($_POST["sign_up_username"]);
                global $sign_up_password;
                $sign_up_password = test_input($_POST["sign_up_password"]);
                global $sign_up_display_name;
                $sign_up_display_name = test_input($_POST["sign_up_displayname"]);
            }
             
            $db = db_connect();
            
            if (isset($db)) {
                $stmt = $db->prepare("INSERT INTO user (username, password, name) VALUES (?, ?, ?)");
                $stmt->execute(array($sign_up_name, $sign_up_password, $sign_up_display_name)); 
                  $_SESSION['signed_in_displayname'] = $sign_up_display_name;
                   header('Location: login_page.php');
                   exit();
            }
        } else { // user is already logged in
            header('Location: login_page.php');
            exit();
        }
    }

?>


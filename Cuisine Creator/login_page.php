<?php
    include 'header.php';

?>
            <tr>
                <td style="width: 1100px; border: 1px white solid; text-align: center;"><?php if (isset($_SESSION['message'])) {echo $_SESSION['message'];} ?></td>
            </tr>
            <tr><!-- row 2 -->
                <td>
                    <form action="login.php" method="post">
                        <h2 style="text-align: center;">Log In</h2>
                        <table class="login_table">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td style="color: white">error message</td>
                                        </tr>
                                        <tr>
                                            <td><p>User Name:</p></td>
                                            <td><input type="text" name="username" placeholder="your name"></td>
                                        </tr>
                                        <tr>
                                            <td><p>Password:</p></td>
                                            <td><input type="password" name="password" placeholder="your password"><span class="error"></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td style="text-align: center;"><button type="submit">Log In</button></td>
                                        </tr>
                                        <tr>
                                            <td style="color: white">error message</td>
                                            <td style="text-align: center; color: red;">
                                                <span class="error">
                                                    <?php if (isset($_SESSION['passERR'])) {echo $_SESSION['passERR'];}?>
                                                </span>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </form>
                </td>
            </tr>
            <tr><!-- row 3 -->
                <td>
                    <form action="login.php" method="post">
                        <h2 style="text-align:center;">Sign Up</h2>
                        <table class="login_table">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td style="color: white">error message</td>
                                        </tr>
                                        <tr>
                                            <td><p>User Name:</p></td>
                                            <td><input type="text" name="sign_up_username" placeholder="your name"></td>
                                        </tr>
                                        <tr>
                                            <td><p>Password:</p></td>
                                            <td><input type="password" name="sign_up_password" placeholder="your password"><span class="error"></td>
                                        </tr>
                                        <tr>
                                            <td><p>Display Name:</p></td>
                                            <td><input type="text" name="sign_up_displayname" placeholder="the name you want displayed"><span class="error"></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td style="text-align: center;"><button type="submit">Sign Up</button></td>
                                        </tr>
                                        <tr>
                                            <td style="color: white">error message</td>
                                            <td style="text-align: center; color: red;">
                                                <span class="error">
                                                    <?php if (isset($_SESSION['signpassERR'])) {echo $_SESSION['signpassERR'];}?>
                                                </span>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                
                            </tr>
                        </table>
                    
                    <?php if(isset($_SESSION['signed_in_displayname'])) {
                        echo '<p style="text-align: center; color: red; font-size: 120%;">Welcome '.$_SESSION['signed_in_displayname'].
                                ', you have successfully registered. Please log in.</p>';
                        
                    }?>
                    </form>
                </td>
            </tr>
            <tr><!-- row 4 -->
                    <td>
                        <p id="div_footer">
                        ALL RIGHTS RESERVED, Copyright 2013 | Cuisine Creator &nbsp;&nbsp; 
                        Website created by Joe Nosov, Thomas Nunn, Cathryn Castillo
                        </p>
                    </td>
            </tr>
         </table>
    </body>
</html>
    
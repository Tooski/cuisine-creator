<?php
    session_start();
    include 'functions.php';
    include 'recipes_of_the_day.php';
    

    if (isset($_SESSION['login'])) {
        $userName = $_SESSION['login'][0];
        $displayName = $_SESSION['login'][1];
    }
    
    if (isset($_POST['logout'])) {
        logout();
    }
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="styles/styles.css">
        <title>Cuisine Creator</title>
    </head>
    <body>
        <table id="master_table">
            <tr><!--row 1-->
                <td>
                    <!--begin page header-->     
                    <table id="nav_table">
                        <tr>
                            <td><a href="index.php"><img src="images/cc_logo.png"></a></td>
                            <td style="color: red;"><a href="index.php">Cuisine Creator</a></td>
                            <?php if (isset($displayName)) {
                                echo '<td style="text-align: center; font-size: 120%; width: 200px; color: green;">Welcome, '.$displayName.'!</td>
                                <td style="text-align: center; font-size: 120%;"><a href="my_account.php">My Account</a></td>
                                <td style="text-align: center; font-size: 120%;">
                                    <form action="header.php" method="post"><button type="submit" name="logout">log out</button></form>
                                </td>';
                            } else {
                                echo '<td class="title_table_blank" style="text-align: center; font-size: 120%; width: 200px;"></td>
                                    <td class="title_table_blank" style="text-align: center; font-size: 120%;">
                                    <td class="title_table_blank" style="text-align: right; font-size: 120%;"><a href="login_page.php">login</a></td>
                                    <td class="title_table_blank" style="text-align: center; font-size: 120%;"><a href="login_page.php">sign up</a></td>';
                            }
                            ?>
                        </tr>
                    </table>
                    <table class="nav_bar">
                        <tr>
                            <td><a href="recipes.php"><div class="nav_div">All Recipes</div></a></td>
                            <td><a href="add_recipes.php"><div class="nav_div">Add Recipes</div></a></td>
                            <td><a href="favorites.php"><div class="nav_div">Favorites</div></a></td>
                            <td><a href="about.php"><div class="nav_div">About Us</div></a></td>
                            <td><a href="contact.php"><div class="nav_div">Contact Us</div></a></td>
                            <td>
                                <form action="recipes.php" method="post">
                                <input type="text" name="search" placeholder="search">
                                </form>
                            </td>
                        </tr>
                    </table>
                    <!--end page header-->
                </td>
            </tr> <!-- end row 1 -->
<?php
include "db_connect.php";
if (isset($_GET['product'])) {
    $product = $_GET['product'];
}
if (isset($_POST['ing'])) {
    $product = $_POST['ing'];
}
$dd = FALSE;

foreach ($db->query("SELECT * FROM product where productName='" . $product . "';") as $row) {
    $dd = TRUE;
}

if (!$dd) {
    header("Location: 404.php");
}
?>
<html>
    <head>
        <style>
            iframe {background-color: white;};
            #start:hover {cursor: pointer;  } 
            input[type=number] { width: 75px;}
            .areatext { width : 100%;height : 100px;resize: none;}
            .inputText { width : 100%;}
            #nutritionTable{border-spacing: 0px; margin: 10px;}
            #nutritionTable td:not(#nutF) { padding: 0px; margin: 0px; border: 0px; border-bottom: 1px solid black;}
            #nutritionTable div { display:inline}
            #tbmain tr td  {padding: 5px;}
            #tbmain { margin: 0px 5px; border: 5px #fdc442 solid; padding: 10px 20px; border-spacing: 0 10px;}
            textarea { resize: none; }
        </style>

        <script>
            function descriptions(val)
            {
                document.getElementById("description").innerHTML = eval("(" + val + ")")['description'];
            }

            function items(val) {
                var arrx = eval("(" + val + ")");
                var table = document.getElementById("ingTable");
                var row = table.insertRow(table.rows.length - 1);
                var itemName = arrx['ingredientName'];
                var c1 = row.insertCell(0);
                c1.innerHTML = arrx['quantity'] + " " + arrx['measurement'] + " of " + itemName;

            }
             

       

            function nutrition(val)
            {

                var arrx = eval("(" + val + ")");
                var s = 1;
                var m = arrx['servingMethod'];
                document.getElementById("servingSize").innerHTML = parseInt(arrx['servingSize']);
                document.getElementById("servingMethod").innerHTML = arrx['servingMethod'];
                var fat = document.getElementById("totalFat");
                fat.innerHTML = parseInt(arrx['fat'] * s);
                document.getElementById("totalFatP").innerHTML = Math.ceil((parseInt(fat.innerHTML) / 65) * 100 * s);
                var cff = document.getElementById("saturatedFat");
                cff.innerHTML = parseInt(arrx['saturatedFat'] * s);
                document.getElementById("saturatedFatP").innerHTML = Math.ceil((parseInt(cff.innerHTML) / 20) * 100 * s);
                cff = document.getElementById("cholesterol");
                cff.innerHTML = parseInt(arrx["cholesterol"] * s);
                document.getElementById("cholesterolP").innerHTML = Math.ceil((parseInt(cff.innerHTML) / 300) * 100 * s);
                cff = document.getElementById("sodium");
                cff.innerHTML = parseInt(arrx["sodium"] * s);
                document.getElementById("sodiumP").innerHTML = Math.ceil((parseInt(cff.innerHTML) / 2400) * 100 * s);
                var carb = document.getElementById("carbs");
                carb.innerHTML = parseInt(arrx["carbs"] * s);
                document.getElementById("carbsP").innerHTML = Math.ceil((parseInt(carb.innerHTML) / 300) * 100 * s);
                var df = document.getElementById("dietaryFiber");
                df.innerHTML = parseInt(arrx["dietaryFiber"] * s);
                document.getElementById("dietaryFiberP").innerHTML = Math.ceil((parseInt(df.innerHTML) / 25) * 100 * s);
                cff = document.getElementById("sugar");
                cff.innerHTML = parseInt(arrx["sugars"] * s);
                var prot = document.getElementById("protein");
                prot.innerHTML = parseInt(arrx["protein"] * s);
                cff = document.getElementById("calories");
                cff.innerHTML = parseInt(fat.innerHTML) * 9 + parseInt(prot.innerHTML) * 4 + (parseInt(carb.innerHTML) - parseInt(df.innerHTML)) * 4;
                cff = document.getElementById("caloriesFromFat");
                cff.innerHTML = parseInt(fat.innerHTML) * 9;

            }

            function star(val)
            {

                if (val === -1) {
                    val = document.getElementById("starcount").value;
                }
                var i;
                for (i = 0; i <= val; i++)
                {
                    document.getElementById("star" + i).src = 'images/stars.png';
                }
                for (; i < 5; i++)
                {
                    document.getElementById("star" + i).src = 'images/starsT.png';
                }

            }


        </script>


    </head>
    <body>
    <center>
        <table style="padding:20px;" id="tbmain" class="tbs" >
            <tr >

                <td>
                    <div id="recipeName"><?php
                        echo "<center><h1 style=\"text-transform: uppercase\">" . $product . "</h1></center>";

                        $star;
                        echo "<center>";
                        foreach ($db->query("SELECT * FROM productUser where productName='" . $product . "';") as $row) {
                            echo "Submitted by: " . $row['username'] . " on " . $row['dateCreated'] . ", " . $row['timeCreated'] . "<br>";
                        }
                        foreach ($db->query("SELECT AVG(stars) FROM review where productName='" . $product . "';") as $row) {

                            if ($row[0]) {
                                for ($i = 0; $i < ceil($row[0]); $i++) {
                                    echo '<img src="images/stars.png" width="20">';
                                }
                                for ($i = ceil($row[0]); $i < 5; $i++) {
                                    echo '<img src="images/starsT.png" width="20">';
                                }
                            }
                        }
                        if (isset($_SESSION['login'][0])) {
                            if ($hasHeader) {
                                echo "<form action='add_favorites.php' method='post'>";
                                echo "<input style=\"position:absolute; visibility:hidden\" name='userName' value ='" . $_SESSION['login'][0] . "'>";
                                echo "<input style=\"position:absolute; visibility:hidden\" name='prodName' value ='" . $product . "'>";
                                $bb = FALSE;
                                foreach ($db->query("SELECT * FROM favorite where productName='" . $product . "' AND username = '" . $_SESSION['login'][0] . "';") as $row) {
                                    $bb = TRUE;
                                }
                                echo "<input style=\"position:absolute; visibility:hidden\" name='val' value ='" . $bb . "'>";

                                if ($bb) {
                                    echo "<input type='submit' value='Remove from Favorites'>";
                                } else {
                                    echo "<input type='submit' value='Add to Favorites'>";
                                }
                                echo "</form>";
                            }
                        } else {
                            echo "<br><br><a href='login_page'>Login to add to favorites!</a>";
                        }
                        echo "</center>";
                        ?></div>
                </td>
            </tr>


            <tr id='desc'>
                <td><p><b>Description:</b></p>
                    <p><div id="description" >
                        <?php
                        $img = FALSE;
                        $desc = FALSE;
                        foreach ($db->query("SELECT * FROM product where productName='" . $product . "';") as $row) {
                            if (isset($row['picSource'])) {
                                $img = TRUE;
                                echo "<img src='uploads/" . $row['picSource'] . "' id='picSource' name='picSource' width='200px' align='left'>";
                            }
                            if (isset($row['description'])) {
                                $desc = TRUE;
                                echo $row['description'];
                            }
                        }
                        if ($img === FALSE && $desc === FALSE) {
                            echo " <script> document.getElementById('desc').parentNode.removeChild(document.getElementById('desc'));</script>";
                        }
                        ?>
                    </div></p>
                    <p></p></td>
            </tr>

            <tr>
                <td>



                    <p><?php
                        $dd = $db->query("SELECT * FROM recipe where recipeName = '" . $product . "';");
                        if ($dd->rowCount()) {
                            echo "<p>All Ingredients:</p>";
                            echo "<center><table width = '100%' id='ingTable'>";
                            foreach ($dd as $row) {
                                $hq = $db->query("SELECT productName FROM product where productName = '" . $row['ingredientName'] . "';");
                                $exists = FALSE;
                                foreach ($hq as $r2) {
                                    $exists = TRUE;
                                }
                                if ($hasHeader && $exists) {
                                    echo "<script type=\"text/javascript\"> items('" . json_encode($row) . "');</script>";
                                } else {
                                    echo "<tr><td>" . $row['quantity'] . " " . $row['measurement'] . " " . $row['ingredientName'] . "</td></tr>";
                                }
                            }
                            echo "</table></center>";
                        }
                        ?></p></td> 
            </tr>

            <tr>
                <td><?php
                    foreach ($db->query("SELECT * FROM directions where productName='" . $product . "';") as $row) {
//                        if(empty($row['direction']))
//                        {
                        echo "<hr width='100%' ><strong><br>Directions: </strong>" . $row['direction'];
//                        }
                    }
                    ?>




                    </p></td>
            </tr>
            <tr  id="nutri">

                <td>
                    <hr width='100%' >
            <center><table width="604" id="nutritionTable">
                    <tr>
                        <td colspan="3" id="nutF"><h2>Nutrition Facts</h2></td>
                    </tr>
                    <tr>
                        <td colspan="3">Serving Size  <div id="servingSize"></div>  <div id="servingMethod"></div>
                        </td>
                    </tr>
                    <tr>
                        <td height="13" colspan="3" bgcolor="#000000"></td>
                    </tr>
                    <tr>
                        <td colspan="3"><strong>Amount Per Serving</strong></td>
                    </tr>
                    <tr>
                        <td colspan="2"><strong>Calories </strong><div id="calories">0</div></td>
                        <td width="330"  align="right">Calories from Fat <div id="caloriesFromFat">0</div></td>
                    </tr>
                    <tr>
                        <td height="4" colspan="3" bgcolor="#000000"></td>
                    </tr>
                    <tr>
                        <td colspan="3" align="right"><strong>% Daily Value*</strong></td>
                    </tr>
                    <tr>
                        <td colspan="2"><strong>Total Fat </strong><div id="totalFat">0</div>g</td>
                        <td align="right"><div id="totalFatP">0</div>%</td>
                    </tr>
                    <tr>
                        <td width="20">&nbsp;</td>
                        <td >Saturated Fat <div id="saturatedFat">0</div>g</td>
                        <td align="right"><div id="saturatedFatP">0</div>%</td>
                    </tr>
                    <tr>
                        <td colspan="2"><strong>Cholesterol </strong><div id="cholesterol">0</div>mg</td>
                        <td align="right"><div id="cholesterolP">0</div>%</td>
                    </tr>
                    <tr>
                        <td colspan="2"><strong>Sodium </strong><div id="sodium">0</div>mg</td>
                        <td align="right"><div id="sodiumP">0</div>%</td>
                    </tr>
                    <tr>
                        <td colspan="2"><strong>Total Carbohydrates </strong><div id="carbs">0</div>g</td>
                        <td align="right"><div id="carbsP">0</div>%</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>Dietary Fiber <div id="dietaryFiber">0</div>g</td>
                        <td align="right"><div id="dietaryFiberP">0</div>%</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td colspan="2">Sugars <div id="sugar">0</div>g</td>
                    </tr>
                    <tr>
                        <td colspan="3"><strong>Protein </strong><div id="protein">0</div>g</td>
                    </tr>

                    <?php
                    $dd = $db->query("SELECT * FROM nutrition where itemName='" . $product . "';");
                    if ($dd->rowCount()) {
                        foreach ($dd as $row) {
                            echo "<script type=\"text/javascript\"> nutrition('" . json_encode($row) . "');</script>";
                        }
                    } else {
                        echo " <script> document.getElementById('nutri').parentNode.removeChild(document.getElementById('nutri'));</script>";
                    }
                    ?>

                </table><div id="websource"><?php
                    foreach ($db->query("SELECT webSource FROM nutritionSource WHERE productName='" . $product . "';") as $row) {
                        if (!empty($row['webSource'])) {
                            echo "Source: <a href='" . $row['webSource'] . "'>" . $row['webSource'] . "</a>";
                        }
                    }
                    ?></div></center></td>
            </tr>
            <?php
            $reviewExists = FALSE;
            $dd = $db->query("SELECT * FROM review WHERE productName = '" . $product . "';");
            $count = $dd->rowCount();
            if ($count > 0) {
                echo "<tr><td>";
                foreach ($dd as $row) {
                    if ($row['username'] === $_SESSION['login'][0]) {
                        $reviewExists = TRUE;
                    }
                    echo "<tr><tr><td><hr width='100%' >";
                    if ($row['review']) {
                        if ($row['username']) {
                            echo "<br>Review Submitted By: " . $row['name'];

                            if ($row['username'] === $_SESSION['login'][0] && $hasHeader) {

                                echo "<form action='delete_review.php' method='post'>"
                                . "<input type='text' name='userName' id='userName' style='position:absolute; visibility:hidden' value='" . $_SESSION['login'][0] . "'>"
                                . "<input type='text' name='prod' id='prod' style='position:absolute; visibility:hidden' value='" . $product . "'>"
                                . "<input type='submit' value='Delete Review' style='display:inline-block'></form>";
                            } else {
                                echo "<br>";
                            }
                        }

                        if ($row['stars']) {
                            echo "User Rating: ";
                            for ($i = 0; $i < 5; $i++) {
                                echo '<img src="images/' . ($i < $row['stars'] ? 'stars.png' : 'starsT.png') . '" width="15">';
                            }

                            echo "<br>";
                        }
                        echo $row['review'];
                        echo "<br>";
                    }
                }
            } else {
                echo "<tr><td>";
            }
            if (isset($_SESSION['login'][0])) {
                if (!$reviewExists && $hasHeader) {
                    echo "<form action='submit_review.php' method='post'>";
                    echo "<input type='text' name='prod' id='prod' style='position:absolute; visibility:hidden' value='" . $product . "'>";
                    if ($count) {
                        echo "<hr width='100%' >";
                    }
                    echo "Review Submitted By: " . $_SESSION['login'][1] . "<br>Your Rating: ";
                    echo "<input type='number' name='starcount' id='starcount' style='position:absolute; visibility:hidden' value=5>";
                    for ($i = 0; $i < 5; $i++) {
                        echo "<img src='images/stars.png' width='15' name='star" . $i . "' id='star" . $i . "' onmouseover='star(" . $i . ")' onclick='document.getElementById(\"starcount\").value = " . $i . "' onmouseout='star(-1)' >";
                    }
                    echo "<br><center><textarea style='width: 75%; height: 100px;' placeholder='Type review here!' name='review' id='review'></textarea></center>";
                    echo "<center><input type='submit' value='Submit Review'></center></form>";
                }
            } else {
                echo "<br><hr width='100%' ><br><center><a href='login_page'>Login to submit review!</a>";
            }
            echo "</td></tr>";
            ?>
            <?php
            $dd = $db->query("SELECT category FROM productCategory WHERE productName = '" . $product . "';");
            $count = $dd->rowCount();
            if ($count) {
                echo "<tr><td><hr width='100%' ><center>Tags: ";
                foreach ($dd as $row) {
                    echo "<label>";
                    if ($hasHeader) {
                        echo "<a href='recipes.php?tag=" . $row['category'] . "'>" . $row['category'] . "</a>";
                    } else {
                        echo $row['category'];
                    }
                    $count--;
                    if ($count !== 0) {
                        echo ", ";
                    }
                    echo "</label>";
                }
                echo"</center><hr width='100%' >";
            }

            //$rows = $db->query("SELECT count(*) FROM category")->fetch()[0];
            ?>
        </table>
    </form>
    <p id="div_footer">
        ALL RIGHTS RESERVED, Copyright 2013 | Cuisine Creator &nbsp;&nbsp; 
        Website created by Joe Nosov, Thomas Nunn, Cathryn Castillo
    </p>
</td>
</tr>
</table>
</body>
</html>
